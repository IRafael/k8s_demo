package com.school.java.k8s_demo.po;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "po_person")
@Getter
@Setter
@NoArgsConstructor
public class PO_Person {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "username")
    private String surname;

}
