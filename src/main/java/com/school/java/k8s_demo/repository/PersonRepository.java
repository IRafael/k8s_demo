package com.school.java.k8s_demo.repository;

import com.school.java.k8s_demo.po.PO_Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<PO_Person, String> {
}
