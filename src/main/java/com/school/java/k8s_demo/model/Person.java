package com.school.java.k8s_demo.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Person {

    private final String name;

    private final String surname;

}
