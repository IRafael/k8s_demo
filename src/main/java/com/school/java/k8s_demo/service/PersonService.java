package com.school.java.k8s_demo.service;

import com.school.java.k8s_demo.model.Person;
import com.school.java.k8s_demo.po.PO_Person;
import com.school.java.k8s_demo.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;

    public String createPerson(final String name, final String surname) {
        final PO_Person poPerson = new PO_Person();
        poPerson.setName(name);
        poPerson.setSurname(surname);
        final PO_Person savedPerson = personRepository.save(poPerson);
        return savedPerson.getId();
    }

    public Person getPersonById(final String id) {
        final Optional<PO_Person> byId = personRepository.findById(id);
        return byId.map(poPerson -> new Person(poPerson.getName(), poPerson.getSurname())).orElse(null);
    }

    public List<Person> getAllPersons() {
        return personRepository.findAll().stream().map(poPerson -> new Person(poPerson.getName(), poPerson.getSurname())).collect(Collectors.toList());
    }

}
