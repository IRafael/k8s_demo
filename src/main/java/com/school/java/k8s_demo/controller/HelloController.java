package com.school.java.k8s_demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/greeting")
public class HelloController {

    @GetMapping(value = "/hello")
    public String getHello() {
        return "Hello, world!";
    }

}
