package com.school.java.k8s_demo.controller;

import com.school.java.k8s_demo.model.Person;
import com.school.java.k8s_demo.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/person")
@RequiredArgsConstructor
public class PersonController {

    private final PersonService personService;

    @GetMapping(value = "/getPerson/{id}")
    public Person getPerson(@PathVariable(name = "id") final String id) {
        return personService.getPersonById(id);
    }

    @GetMapping(value = "/createPerson")
    public String createPerson(@RequestParam(name = "name") final String name, @RequestParam(name = "surname") final String surname) {
        return personService.createPerson(name, surname);
    }

    @GetMapping(value = "/getAllPersons")
    public List<Person> getAllPersons() {
        return personService.getAllPersons();
    }

}
