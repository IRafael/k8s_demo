# Demo application for joint k8s and Spring work

## Requirements
  - ### [Java 17](https://jdk.java.net/java-se-ri/17)
  - ### [Docker](https://docs.docker.com/)

# How to

- ## Build
  > ### Important notice. For now database disabled.
  - ### Go to root dir
  - ### Run `./mvnw clean package` in terminal

- ## Run
  - ### Go to the target directory `cd target/`
  - ### Run in terminal  `java -jar k8s_demo-0.0.1-SNAPSHOT.jar` 

- ## Build docker image
  - ### Run in terminal in root directory  `docker build --build-arg JAR_FILE=target/*.jar -t k8s-spring/example .`

- ## Run docker container
  - ### Run in terminal in `docker run k8s-spring/example`

# Useful commands
- ### `docker` print docker help information
- ### `docker <command> --help'` print docker help information about command
- ### `docker image ls` print all docker images
- ### `docker ps` print all running docker containers
- ### `docker ps -a` print all docker containers
- ### `docker run <user-name>/<image-name>:<tag-name>` create and start container
- ### `docker stop <container-id>` stop existing container
- ### `docker start <container-id>` start existing container
- ### `docker logs <container-id>` print logs from container
- ### `docker rm <container-id>` remove existing container
- ### `docker exec <container-id> <command>` execute command in container
- ### `docker image rm <user-name>/<image-name>:<tag-name>` delete docker image
- ### `docker tag <source-image>[:tag] <target-image>[:tag]` add new tag for image
- ### `docker push <source-image>[:tag]` push image in remote repository
- ### `docker login` login in docker hub